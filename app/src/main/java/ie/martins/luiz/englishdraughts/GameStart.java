package ie.martins.luiz.englishdraughts;

import android.util.Log;
// ---- Hashsets are being used instead of arrays basically to avoid duplicated values
import java.util.HashSet;

public class GameStart {

    public GameStart() {
        gameRules = new GameRules();
        // ---- Those two hashset are used to save the possible moves and possible eats for the current action. ----
        moveCellsHashSet = new HashSet();
        eatCellsHashSet = new HashSet();
    }

    static GameRules gameRules;
    static Cell selectedCell, selectedCellBackup, currentCell;
    static HashSet moveCellsHashSet, eatCellsHashSet;
    // ---- Those counters are used to calculate the possible eats, 0 = nothing, 1 = there is a possibility on the board
    //      >1 the current cell can do it, it's being used mostly to block the moves when we can eat
    static Integer validEatCounter;
    // ---- It's similar to the variable above the difference is 0 = nothing, 1 = one possible move for the current cell
    //      2 = two possible moves for the current cell and so on, there is no scanning on the whole board
    static Integer validMovesCounter;
    // ---- The variable below is correcting a bug found on the application affecting the position [0x,0y] on the board.
    //      Bug needs to be understood to remove this fix.
    static Integer zeroZeroFix;

    static public void touchAction(int x, int y) {
        Log.i("touchAction", "*" + x + "*" + y + "*");
        // ---- The validEatCounter is a counter incremented if there is a eat possibility on the whole board
        //      The zeroZeroFix is a fix to a nullpointer exception which was ocurring when processing the position [0,0]
        //      To fix that the position [0,0] is bypassed on the first run and after the [0,1] we go back to the [0,0]
        //      it's just a way to fix the problem and avoid the nullpointer exception.
        validEatCounter = 0; validMovesCounter=0; zeroZeroFix =0;

        // ---- Creating a backup of the selectedCell before we start the verification of possible eats on the whole board
        //      the selected cell needs to be changed to simulate each position on the board ----
        selectedCellBackup = selectedCell;

        // ---- Those for loops will scan the whole board verifying if there is a eat possibility incrementing the validEatCounter
        //      which will disable the movement rules afterwards, no moves or eats will be made, it's just a scanning ----
        // ----- A more detailed explanation of each step after this scanning part on the bottom ----
            for (int i = 0; i < Board.boardSize; ++i) {
                for (int ii = 0; ii < Board.boardSize; ++ii) {
                    // ---- The [0,0] will be bypassed once, look at the end of the for loop to see why ----
                    if (i == 0 && ii == 0 && zeroZeroFix == 0) {
                        ii = 1;
                    }
                        currentCell = GameRules.returnBoard().returnCellFromBoard(i, ii);
                        if (GameRules.includeValidEatCells(selectedCell, eatCellsHashSet) || GameRules.includeValidEatCellsQueen(selectedCell, eatCellsHashSet)) {
                            GameRules.executeEat(selectedCell, currentCell);
                            validEatCounter = validEatCounter + 1;
                        } else {
                            if (validEatCounter == 0) {
                                GameRules.executeEat(selectedCell, currentCell);
                                GameRules.executeMovement(selectedCell, currentCell);
                            }
                        }
                        selectedCell = null;
                        moveCellsHashSet.clear();
                        eatCellsHashSet.clear();
                        if (GameRules.returnMoveStones().contains(currentCell)) {
                            selectedCell = currentCell;
                            if (validEatCounter == 0) {
                                GameRules.includeValidMovementCells(selectedCell, moveCellsHashSet);
                            }
                            GameRules.includeValidEatCells(selectedCell, eatCellsHashSet);
                        }

                        // When on position [0,1] go back to [0,-1], which will be incremented in the end of the for loop to [0,0].
                        if (i == 0 && ii == 1 && zeroZeroFix == 0) {
                            ii = ii - 2;
                            zeroZeroFix++;
                        }
                }
            }

        // ---- Here we have finished the scan on the whole board for eats and the following lines will make the moves/eats ----

        // ---- Return the value from the backup to the selectedCell after the eats verification ----
        selectedCell = selectedCellBackup;

        //---- The touched x,y coordinates will be saved on the currentCell variable for future use
        currentCell = GameRules.returnBoard().returnCellFromBoard(x, y);
        //---- If the cell was already selected and there are valid eats for normal or queen we can execute the eat
        if (GameRules.includeValidEatCells(selectedCell, eatCellsHashSet) || GameRules.includeValidEatCellsQueen(selectedCell, eatCellsHashSet)) {
            GameRules.executeEat(selectedCell, currentCell);
            validEatCounter = validEatCounter + 1;
        }
        //---- If the cell was already selected and there are no valid eats we can move
        else {
            if (validEatCounter == 0) {
                GameRules.executeMovement(selectedCell, currentCell);
            }
        }
        //---- After the move of the selected cell above we'll clear the selected cell and the possible moves and eats
        selectedCell=null;
        moveCellsHashSet.clear();
        eatCellsHashSet.clear();
        //---- If the currentCell is a stone from the current player it will be selected and if there isn't possible eats on the
        //      board the hashset including the possible moves will be filled in
        //      If there are possible eats only the hashset for possible eats will be filled in ----
        if (GameRules.returnMoveStones().contains(currentCell)) {
            selectedCell = currentCell;
            if (validEatCounter == 0) {
                GameRules.includeValidMovementCells(selectedCell, moveCellsHashSet);
            }
            GameRules.includeValidEatCells(selectedCell, eatCellsHashSet);
        }
    }

    static public boolean returnOnlyIfSelectedCell(Cell cell) {
        if (GameRules.includeValidEatCells(selectedCell, eatCellsHashSet)){
            return selectedCell == cell || eatCellsHashSet.contains(cell);
        }
        else {
            return selectedCell == cell || eatCellsHashSet.contains(cell) || moveCellsHashSet.contains(cell);
        }
    }
}