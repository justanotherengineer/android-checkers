package ie.martins.luiz.englishdraughts;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import java.util.Random;

// ---- AppCompatActivity doesn't work properly with the onCreate(new Bundle()), using Activity instead. ----
public class MainActivity extends Activity {
    private GameView gameView;
    static Button darkForces;
    static TextView redPlayerCount, whitePlayerCount, gameStatus;
    static Chronometer chronometer;
    //---- The variable below is being used on the gameMode, its value is defined on the onMeasure in the GameView view----
    static int gameViewDiameter;
    static Spinner spinnernewgame, spinnernewgame3moves;
    private String selectedGameMode;
    private Integer random;

    //---------------Part of the code of the first three moves must be improved if time allows ***No time due othe
    //               assignments*** isn't organized but works fine --------------
    private Integer y,y2,y3,x6,x5,x4,x3,x2,x,xremainder,yremainder,xremainder2,yremainder2,xremainder3,yremainder3;
    //------------------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // ---- Get access to the views ----
        gameView = (GameView) findViewById(R.id.gameview);
        spinnernewgame = (Spinner) findViewById(R.id.spinnernewgame);
        spinnernewgame3moves = (Spinner) findViewById(R.id.spinnernewgame3moves);
        darkForces = (Button) findViewById(R.id.darkforces);
        chronometer = (Chronometer) findViewById(R.id.chronometer);
        redPlayerCount = (TextView) findViewById(R.id.tvredplayercount);
        whitePlayerCount = (TextView) findViewById(R.id.tvwhiteplayercount);
        gameStatus = (TextView) findViewById(R.id.tvgamestatus);
        //The GameView sound effect needs access tho the MainActivity's context
        GameView.mainActivityContextThis = MainActivity.this;

        //----Pulling the boardsizes array(argamemodes) from the XML resources and linking to the
        //    Array adapter to be used with the spinner----
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this,
                R.array.argamemodes,
                android.R.layout.simple_spinner_item);
        //----Specifying the layout that will be used on the list of choices when the spinner is clicked----
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_activated_1);
        //----Setting the Spinner's adapter----
        spinnernewgame.setAdapter(adapter);
        spinnernewgame.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            //----Method that will be called when an item has been selected----
            public void onItemSelected(AdapterView<?> parent, View view, final int pos, long id) {
                //----Saving the selectedGameMode for future use----
                selectedGameMode = spinnernewgame.getSelectedItem().toString();
                //----The available board sizes are from 6 to 20 according to the XML.----
                for (int i = 6; i <= 20; i++) {
                    //----Replacing the non-integer default value "Please Select" to 0 to avoid problems on for loop----
                    if (Integer.parseInt(selectedGameMode.replaceAll("Please Select", "0")) == i)
                        //----Depending on the value of the XML array we'll start in a different gameMode(Board size)----
                        gameMode(i);
                }
            }

            //----Overridden method that will be called when no item has been selected in the spinner----
            public void onNothingSelected(AdapterView<?> parent) {
                //----Here is where we would react if no item was selected, we won't do anything----
            }
        });
        //----On the following lines we are defining a second spinner, executing the first three moves----
        //----Setting the Spinner's adapter----
        spinnernewgame3moves.setAdapter(adapter);
        spinnernewgame3moves.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            //----Method that will be called when an item has been selected----
            public void onItemSelected(AdapterView<?> parent, View view, final int pos, long id) {
                //----Saving the selectedGameMode for future use----
                selectedGameMode = spinnernewgame3moves.getSelectedItem().toString();
                //----The available board sizes are from 6 to 20 according to the XML.----
                for (int i = 6; i <= 20; i++) {
                    //----Replacing the non-integer default value "Please Select" to 0 to avoid problems on for loop----
                    if (Integer.parseInt(selectedGameMode.replaceAll("Please Select", "0")) == i)
                        //----Depending on the value of the XML array we'll start in a different gameMode(Board size) with the
                        //    initial three moves----
                        gameMode3moves(i);
                }
            }
            //----Overridden method that will be called when no item has been selected in the spinner----
            public void onNothingSelected(AdapterView<?> parent) {
                //----Here is where we would react if no item was selected, we won't do anything----
            }
        });

        //----Update the Red players stone count----
        GameRules.returnRedStoneCount();
        //----Update the White players stone count----
        GameRules.returnWhiteStoneCount();
        //----Displaying the value from the functions above----
        MainActivity.redPlayerCount.setText("RED: " + GameView.redStoneCount);
        MainActivity.whitePlayerCount.setText("WHITE: " + GameView.whiteStoneCount);
        //----Displaying the player turn if RED or WHITE----
        if (PlayerEnum.returnInversePlayer(GameRules.currentPlayer) == PlayerEnum.WHITE) {
            MainActivity.gameStatus.setText("Turn: " + PlayerEnum.returnInversePlayer(GameRules.currentPlayer));
            MainActivity.gameStatus.setTextColor(Color.WHITE);
        }
        if (PlayerEnum.returnInversePlayer(GameRules.currentPlayer) == PlayerEnum.RED) {
            MainActivity.gameStatus.setText("Turn: " + PlayerEnum.returnInversePlayer(GameRules.currentPlayer));
            MainActivity.gameStatus.setTextColor(Color.RED);
        }
        //----If there is no stone left, red or white inform the winner----
        if (GameView.redStoneCount == 0) {
            MainActivity.gameStatus.setText("Status: WHITE WINS!!!");
        }
        if (GameView.whiteStoneCount == 0) {
            MainActivity.gameStatus.setText("Status: RED WINS!!!");
        }
        //----When the app starts the chronometer will start automatically----
        chronometer.start();

        //----It will define the Dark Forces button which will change the colours and sound of the game
        //    the onDraw on GameView will verify the current text of the button to invalidade the view properly----
        darkForces.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (darkForces.getText().equals("DARK FORCES OFF")) {
                    darkForces.setText("DARK FORCES ON");
                    darkForces.setTextColor(Color.GREEN);
                } else {
                    darkForces.setText("DARK FORCES OFF");
                    darkForces.setTextColor(Color.parseColor("#fdd078"));
                }
                //----Invalidade is necessary to force an update right now.----
                gameView.invalidate();
            }
        });
    }

    //----That random function generates a random number from "from" to "to", i.e 0 to 8, including 0 and 8.----
    public Integer random(int from, int to) {
        Random r = new Random();
        int random = r.nextInt((to + 1) - from);
        return random;
    }

    //----Here we start the game according to a specific dimension executing the onCreate to restart the activity----
    public void gameMode(int dimension) {
        //----It will define the game mode i.e 6=6x6, 8=8x8 and 20=20x20----
        Board.boardSize = dimension;
        /*---- The Bundle in the onCreate method should hold the state of you activity before it was killed.
               Creating a new Bundle should clean that and start again ----*/
        onCreate(new Bundle());
        //----Even with the onMeasure in the GameView we need to resize the gameView manually after the onCreate(new Bundle())----
        //----Because the gameView starts occupying the whole screen and until the onMeasure run some parts of the screen will blink----
        gameView.setLayoutParams(new LinearLayout.LayoutParams(gameViewDiameter, gameViewDiameter));
        //----Restart the chronometer----
        chronometer.start();
    }

    //----The same idea of the function gameMode below, but executing the first three moves automatically----
    public void gameMode3moves(int dimension) {
        Board.boardSize = dimension;
        onCreate(new Bundle());
        gameView.setLayoutParams(new LinearLayout.LayoutParams(gameViewDiameter, gameViewDiameter));
        chronometer.start();

        //----Coding for the first three moves, everything working(all bugs fixed), needs to be optimized, additional functions would be grand----
        //----It's simulating all possible user actions for the first three moves using a random method
        //----First touch event, selecting the RED stone----
        y = (Board.boardSize - ((Board.boardSize / 2) - 1));
        yremainder = y % 2;
        if (yremainder == 0) {
            x = random(0, Board.boardSize - 2);
            xremainder = x % 2;
            if (xremainder != 0) {
                x = x + xremainder;
            }
        }
        if (yremainder == 1) {
            x = random(1, Board.boardSize - 1);
            xremainder = x % 2;
            if (xremainder == 0) {
                x = x + 1;
            }
        }
        GameStart.touchAction(x, y);

        //----Second touch event, moving the RED stone----
        if (yremainder == 0) {
            if (x > 0) {
                x2 = random(0, 1);
                if (x2 == 0) {
                    x2 = x - 1;
                } else {
                    x2 = x + 1;
                }
            }
            if (x == 0) {
                x2 = x + 1;
            }
        }
        if (yremainder != 0) {
            if (x < Board.boardSize - 1) {
                x2 = random(0, 1);
                if (x2 == 0) {
                    x2 = x - 1;
                } else {
                    x2 = x + 1;
                }
            }
            if (x == Board.boardSize - 1) {
                x2 = x - 1;
            }
        }
        GameStart.touchAction(x2, (Board.boardSize - (Board.boardSize / 2)));

        //----Third touch event, selecting the WHITE stone----
        y2 = ((Board.boardSize / 2) - 2);
        yremainder2 = y2 % 2;
        if (yremainder2 == 0) {
            x3 = random(0, Board.boardSize - 2);
            xremainder2 = x3 % 2;
            if (xremainder2 != 0) {
                x3 = x3 + xremainder2;
            }
        }
        if (yremainder2 == 1) {
            x3 = random(1, Board.boardSize - 1);
            xremainder2 = x3 % 2;
            if (xremainder2 == 0) {
                x3 = x3 + 1;
            }
        }
        GameStart.touchAction(x3, y2);

        //----Forth touch event, moving the WHITE stone----
        if (yremainder2 == 0) {
            if (x3 > 0) {
                x4 = random(0, 1);
                if (x4 == 0) {
                    x4 = x3 - 1;
                } else {
                    x4 = x3 + 1;
                }
            }
            if (x3 == 0) {
                x4 = x3 + 1;
            }
        }
        if (yremainder2 != 0) {
            if (x3 < Board.boardSize - 1) {
                x4 = random(0, 1);
                if (x4 == 0) {
                    x4 = x3 - 1;
                } else {
                    x4 = x3 + 1;
                }
            }
            if (x3 == Board.boardSize - 1) {
                x4 = x3 - 1;
            }
        }
        GameStart.touchAction(x4, ((Board.boardSize / 2) - 1));

        //----Fifth touch event, selecting the RED stone----
        y3 = random(0, 2);
        if (y3 == 0) {
            y3 = (Board.boardSize - (Board.boardSize / 2));
        }
        else if (y3 == 1){
            y3 = (Board.boardSize - ((Board.boardSize / 2) - 1));
        }
        else {
            y3 = (Board.boardSize - ((Board.boardSize / 2) - 2));
        }
        yremainder3 = y3 % 2;
        if (yremainder3 == 0) {
            x5 = random(0, Board.boardSize - 2);
            xremainder3 = x5 % 2;
            if (xremainder3 != 0) {
                x5 = x5 + xremainder3;
            }
        }
        if (yremainder3 == 1) {
            x5 = random(1, Board.boardSize - 1);
            xremainder3 = x5 % 2;
            if (xremainder3 == 0) {
                x5 = x5 + 1;
            }
        }
        GameStart.touchAction(x5, y3);
        while (GameStart.validEatCounter == 0 && (GameStart.moveCellsHashSet.size() == 0 || Board.returnCellFromBoard(x5, y3).returnOnlyIfFreeCell())) {
            y3 = random(0, 2);
            if (y3 == 0) {
                y3 = (Board.boardSize - (Board.boardSize / 2));
            }
            else if (y3 == 1){
                y3 = (Board.boardSize - ((Board.boardSize / 2) - 1));
            }
            else {
                y3 = (Board.boardSize - ((Board.boardSize / 2) - 2));
            }
            yremainder3 = y3 % 2;
            if (yremainder3 == 0) {
                x5 = random(0, Board.boardSize - 2);
                xremainder3 = x5 % 2;
                if (xremainder3 != 0) {
                    x5 = x5 + xremainder3;
                }
            }
        if (yremainder3 == 1) {
            x5 = random(1, Board.boardSize - 1);
            xremainder3 = x5 % 2;
            if (xremainder3 == 0) {
                x5 = x5 + 1;
            }
        }
            GameStart.touchAction(x5, y3);
            GameStart.touchAction(x5, y3);
    }
            while (GameStart.validEatCounter > 0 && GameStart.validEatCounter < 2) {
                y3 = random(0, 2);
                if (y3 == 0) {
                    y3 = (Board.boardSize - (Board.boardSize / 2));
                } else if (y3 == 1) {
                    y3 = (Board.boardSize - ((Board.boardSize / 2) - 1));
                } else {
                    y3 = (Board.boardSize - ((Board.boardSize / 2) - 2));
                }
                yremainder3 = y3 % 2;

                if (yremainder3 == 0) {
                    x5 = random(0, Board.boardSize - 2);
                    xremainder3 = x5 % 2;
                    if (xremainder3 != 0) {
                        x5 = x5 + xremainder3;
                    }
                }
                if (yremainder3 == 1) {
                    x5 = random(1, Board.boardSize - 1);
                    xremainder3 = x5 % 2;
                    if (xremainder3 == 0) {
                        x5 = x5 + 1;
                    }
                }
                GameStart.touchAction(x5, y3);
                GameStart.touchAction(x5, y3);
            }

        //----Sixth and last touch event, moving the RED stone. There are many lines because three rows are involved,
        //    it is possible to eat stones depending on the random position of both players and if the is the
        //    option to eat you must take it----
            if (yremainder3 == 0) {
                if (x5 > 0) {
                    x6 = random(0, 1);
                    if (x6 == 0 )  {
                       if (Board.returnCellFromBoard((x5 - 1), (y3 - 1)).returnOnlyIfFreeCell() && GameStart.validEatCounter == 0 && GameRules.currentPlayer == PlayerEnum.WHITE) {
                           x6 = x5 - 1;
                           GameStart.touchAction(x6, (y3 - 1));
                       }
                        if (Board.returnCellFromBoard((x5 + 1), (y3 - 1)).returnOnlyIfFreeCell() && GameStart.validEatCounter == 0 && GameRules.currentPlayer == PlayerEnum.WHITE) {
                            x6 = x5 + 1;
                            GameStart.touchAction(x6, (y3 - 1));
                        }
                        if (!Board.returnCellFromBoard((x5 - 1), (y3 - 1)).returnOnlyIfFreeCell() && GameStart.validEatCounter != 0 && GameRules.currentPlayer == PlayerEnum.WHITE) {
                            x6 = x5 - 2;
                            GameStart.touchAction(x6, (y3 - 2));
                        }
                        if (!Board.returnCellFromBoard((x5 + 1), (y3 - 1)).returnOnlyIfFreeCell() && GameStart.validEatCounter != 0 && GameRules.currentPlayer == PlayerEnum.WHITE) {
                            x6 = x5 + 2;
                            GameStart.touchAction(x6, (y3 - 2));
                        }
                    }
                    if (x6 == 1 ){
                        if (Board.returnCellFromBoard((x5 + 1), (y3 - 1)).returnOnlyIfFreeCell() && GameStart.validEatCounter == 0 && GameRules.currentPlayer == PlayerEnum.WHITE) {
                            x6 = x5 + 1;
                            GameStart.touchAction(x6, (y3 - 1));
                        }
                        if (Board.returnCellFromBoard((x5 - 1), (y3 - 1)).returnOnlyIfFreeCell() && GameStart.validEatCounter == 0 && GameRules.currentPlayer == PlayerEnum.WHITE) {
                            x6 = x5 - 1;
                            GameStart.touchAction(x6, (y3 - 1));
                        }
                        if (!Board.returnCellFromBoard((x5 + 1), (y3 - 1)).returnOnlyIfFreeCell() && GameStart.validEatCounter != 0 && GameRules.currentPlayer == PlayerEnum.WHITE) {
                            x6 = x5 + 2;
                            GameStart.touchAction(x6, (y3 - 2));
                        }
                        if (!Board.returnCellFromBoard((x5 - 1), (y3 - 1)).returnOnlyIfFreeCell() && GameStart.validEatCounter != 0 && GameRules.currentPlayer == PlayerEnum.WHITE) {
                            x6 = x5 - 2;
                            GameStart.touchAction(x6, (y3 - 2));
                        }
                    }
                }
                if (x5 == 0 ) {
                    if (Board.returnCellFromBoard((x5 + 1), (y3 - 1)).returnOnlyIfFreeCell() && GameStart.validEatCounter == 0 && GameRules.currentPlayer == PlayerEnum.WHITE) {
                        x6 = x5 + 1;
                        GameStart.touchAction(x6, (y3 - 1));
                    }
                    if (GameStart.validEatCounter != 0 && GameRules.currentPlayer == PlayerEnum.WHITE) {
                        x6 = x5 + 2;
                        GameStart.touchAction(x6, (y3 - 2));
                    }
                }
            }
            if (yremainder3 != 0) {
                if (x5 < Board.boardSize - 1) {
                    x6 = random(0, 1);
                    if (x6 == 0 ) {
                        if ( Board.returnCellFromBoard((x5 - 1), (y3 - 1)).returnOnlyIfFreeCell() && GameStart.validEatCounter == 0 && GameRules.currentPlayer == PlayerEnum.WHITE) {
                            x6 = x5 - 1;
                            GameStart.touchAction(x6, (y3 - 1));
                        }
                        if (Board.returnCellFromBoard((x5 + 1), (y3 - 1)).returnOnlyIfFreeCell() && GameStart.validEatCounter == 0 && GameRules.currentPlayer == PlayerEnum.WHITE) {
                            x6 = x5 + 1;
                            GameStart.touchAction(x6, (y3 - 1));
                        }
                        if (!Board.returnCellFromBoard((x5 - 1), (y3 - 1)).returnOnlyIfFreeCell() && GameStart.validEatCounter != 0 && GameRules.currentPlayer == PlayerEnum.WHITE) {
                            x6 = x5 - 2;
                            GameStart.touchAction(x6, (y3 - 2));
                        }
                        if (!Board.returnCellFromBoard((x5 + 1), (y3 - 1)).returnOnlyIfFreeCell() && GameStart.validEatCounter != 0 && GameRules.currentPlayer == PlayerEnum.WHITE) {
                            x6 = x5 + 2;
                            GameStart.touchAction(x6, (y3 - 2));
                        }
                    }
                    if (x6 == 1 )  {
                        if (Board.returnCellFromBoard((x5 + 1), (y3 - 1)).returnOnlyIfFreeCell() && GameStart.validEatCounter == 0 && GameRules.currentPlayer == PlayerEnum.WHITE) {
                            x6 = x5 + 1;
                            GameStart.touchAction(x6, (y3 - 1));
                        }
                        if ( Board.returnCellFromBoard((x5 - 1), (y3 - 1)).returnOnlyIfFreeCell() && GameStart.validEatCounter == 0 && GameRules.currentPlayer == PlayerEnum.WHITE) {
                            x6 = x5 - 1;
                            GameStart.touchAction(x6, (y3 - 1));
                        }
                        if (!Board.returnCellFromBoard((x5 + 1), (y3 - 1)).returnOnlyIfFreeCell() && GameStart.validEatCounter != 0 && GameRules.currentPlayer == PlayerEnum.WHITE) {
                            x6 = x5 + 2;
                            GameStart.touchAction(x6, (y3 - 2));
                        }
                        if (!Board.returnCellFromBoard((x5 - 1), (y3 - 1)).returnOnlyIfFreeCell() && GameStart.validEatCounter != 0 && GameRules.currentPlayer == PlayerEnum.WHITE) {
                            x6 = x5 - 2;
                            GameStart.touchAction(x6, (y3 - 2));
                        }
                    }
                }
                if (x5 == (Board.boardSize - 1)) {
                    if (Board.returnCellFromBoard((x5 - 1), (y3 - 1)).returnOnlyIfFreeCell() && GameStart.validEatCounter == 0 && GameRules.currentPlayer == PlayerEnum.WHITE) {
                        x6 = x5 - 1;
                        GameStart.touchAction(x6, (y3 - 1));
                    }
                    if (GameStart.validEatCounter != 0 && GameRules.currentPlayer == PlayerEnum.WHITE) {
                        x6 = x5 - 2;
                        GameStart.touchAction(x6, (y3 - 2));
                    }
                }
            }

        //----Update the Red players stone count----
        GameRules.returnRedStoneCount();
        //----Update the White players stone count----
        GameRules.returnWhiteStoneCount();
        //----Displaying the value from the functions above and the current player turn----
        MainActivity.redPlayerCount.setText("RED: " + GameView.redStoneCount);
        MainActivity.whitePlayerCount.setText("WHITE: " + GameView.whiteStoneCount);
        if (PlayerEnum.returnInversePlayer(GameRules.currentPlayer) == PlayerEnum.WHITE){
            MainActivity.gameStatus.setText("Turn: " + PlayerEnum.returnInversePlayer(GameRules.currentPlayer));
            MainActivity.gameStatus.setTextColor(Color.WHITE);
        }
        if (PlayerEnum.returnInversePlayer(GameRules.currentPlayer) == PlayerEnum.RED){
            MainActivity.gameStatus.setText("Turn: " + PlayerEnum.returnInversePlayer(GameRules.currentPlayer));
            MainActivity.gameStatus.setTextColor(Color.RED);
        }
    }
}