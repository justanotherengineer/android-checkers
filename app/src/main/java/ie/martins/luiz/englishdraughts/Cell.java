package ie.martins.luiz.englishdraughts;

// ---- This class defines the cells ----
public class Cell {

    // ---- In each cell we have the following values, it's positions X and Y and the stone which is in there ----
    public Stone stone;
    public int X, Y;

    // ----- When Cell(x,y) is called the values are stored ----
    public Cell(int x, int y) {
        stone = null;
        X = x;
        Y = y;
    }

    // ---- It will return the position X which was stored ----
    public int returnXpos() {
        return X;
    }

    // ---- It will return the position Y which was stored ----
    public int returnYpos() {
        return Y;
    }

    // ---- It will return the stone which was stored ----
    public Stone returnStone() {
        return stone;
    }

    // ---- It will check if the cell is occupied and if it has the same player from the input ----
    public boolean returnOnlyIfhasPlayer(PlayerEnum player) {
        return !returnOnlyIfFreeCell() && this.stone.returnPlayer() == player;
    }

    // ---- Check if the cell is free ----
    public boolean returnOnlyIfFreeCell() {
        return stone == null;
    }

    // ---- Set the Cell as a free/empty cell ----
    public void setFreeCell() {
        stone = null;
    }

    // ---- Set the stone as the same stone informed on the input ----
    public void setStone(Stone stone) {
        this.stone = stone;
    }
}
