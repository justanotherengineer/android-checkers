package ie.martins.luiz.englishdraughts;

// ---- This class is responsible to define different types of Stones ----
public class Stone {

    //**** The variables are on the bottom ****

    // ---- It will save the informed player on the player variable for future use ----
    public Stone(PlayerEnum player) {
        Stone.this.player = player;
    }

    // ---- It will return the PlayerEnum value WHITE ----
    public static Stone returnWhiteStone() {
        return whiteStone;
    }

    // ---- It will return the PlayerEnum value RED ----
    public static Stone returnRedStone() {
        return redStone;
    }

    // ---- It will return the PlayerEnum value WHITEQUEEN ----
    public static Stone returnWhiteStoneQueen() {
        return whiteStoneQueen;
    }

    // ---- It will return the PlayerEnum value REDQUEEN ----
    public static Stone returnRedStoneQueen() {
        return redStoneQueen;
    }

    // ---- It will return the player which is saved on the player variable ----
    public PlayerEnum returnPlayer() {
        return player;
    }

    // ---- It will check if the player saved on the player variable is the RED if positive it will return ----
    public boolean returnOnlyIfRed() {
        return player == PlayerEnum.RED;
    }

    // ---- It will check if the player saved on the player variable is the WHITE if positive it will return ----
    public boolean returnOnlyIfWhite() {
        return player == PlayerEnum.WHITE;
    }

    // ---- It will check if the player saved on the player variable is the REDQUEEN if positive it will return ----
    public boolean returnOnlyIfRedQueen() {
        return player == PlayerEnum.REDQUEEN;
    }

    // ---- It will check if the player saved on the player variable is the WHITEQUEEN if positive it will return ----
    public boolean returnOnlyIfWhiteQueen() {
        return player == PlayerEnum.WHITEQUEEN;
    }

    public PlayerEnum player;
    public static Stone whiteStone = new Stone(PlayerEnum.WHITE);
    public static Stone redStone = new Stone(PlayerEnum.RED);
    public static Stone redStoneQueen = new Stone(PlayerEnum.REDQUEEN);
    public static Stone whiteStoneQueen = new Stone(PlayerEnum.WHITEQUEEN);
}
